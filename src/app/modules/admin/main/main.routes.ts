import { Routes } from '@angular/router';
import { MainComponent } from 'app/modules/admin/main/main.component';

export default [
    {
        path     : '',
        component: MainComponent,
    },
] as Routes;
