import { NgClass, NgFor, NgIf } from "@angular/common";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatMenuModule } from "@angular/material/menu";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatTableModule } from "@angular/material/table";
import { MatTabsModule } from "@angular/material/tabs";
import { MatChipsModule } from "@angular/material/chips";
import { NgxSpinnerModule } from "ngx-spinner";
import { FuseLoadingBarComponent } from "@fuse/components/loading-bar";
import { RouterLink, RouterOutlet } from "@angular/router";
import { hospital, visit, visit_detail } from "./data";

import { ListHospitalService } from "../../../services/list-hospital.service";
import { ListVisitService } from "../../../services/list-visit.service";
import { ListServiceProfileService } from "../../../services/list-serviceprofile.service";
import { AllergyService } from "../../../services/allergy.service";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { SweetAlertService } from "../../../services/sweetalert.service";
import { LogsService } from "../../../services/logs.service";
import { SecureAgentService } from "app/services/secure-agent.service";

// 3460300244658
// 3340101260778   3349900138297  3342000107828  1348500064329

export const profile = [
  {
    cid: "1234567890123",
    first_name: "xxxxx",
    last_name: "xxxxx",
    birth: "01/01/2020",
    age: "30",
    sex: "xxx",
    selected_dateserv: "xxx",
    selected_timeserv: "xxx",
    selected_namecln: "xxx",
    pttype_name: "xxx",
    address: "123 ถนน สุขุมวิท แขวง คลองเตย เขต คลองเตย กรุงเทพมหานคร 10110",
    tel: "02-123-4567",
    warfarin: false,
    steptokinese: "2023-01-01",
  },
];

@Component({
  selector: "main",
  templateUrl: "./main.component.html",
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.Default,
  standalone: true,
  imports: [
    MatSidenavModule,
    NgIf,
    NgxSpinnerModule,
    MatChipsModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatTabsModule,
    NgFor,
    NgClass,
    RouterLink,
    RouterOutlet,
  ],
})
export class MainComponent {
  hospitals: any = [];
  visits: any = [];
  select_hospcode: any = "";
  select_visit: any = "";
  select_cid: any = "";
  patient: any;
  cc: any;
  pi: any;
  ph: any;
  pe: any;
  bmi: any;
  bt: any;
  dbp: any;
  sbp: any;
  pr: any;
  rr: any;
  wt: any;
  ht: any;
  o2sat: any;
  eye_score: any;
  gcs_score: any;
  verbal_score: any;
  detail: any;
  vital_sign: any;
  is_admit: boolean = false;
  selected_namecln: any = "";
  selected_dateserv: any = "";
  selected_timeserv: any = "";
  diagnosis: any = [];
  xrays: any = [];
  drugs: any = [];
  procedures: any = [];
  labs: any = [];
  is_display = false;
  allergy: any = [];
  userFullNam:any='';
  orders: any = [];

  /**
   * Constructor
   */
  constructor(
    private listHospitalService: ListHospitalService,
    private listVisitService: ListVisitService,
    private listServiceProfileService: ListServiceProfileService,
    private allergyService: AllergyService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private sweetAlertService: SweetAlertService,
    private logsService: LogsService,
    private secureAgentService: SecureAgentService
  ) {
    // this.hospitals = hospital;
    this.patient = {};
    if (sessionStorage.getItem("token")) {
      sessionStorage.setItem("accessToken", sessionStorage.getItem("token"));
    } else if (sessionStorage.getItem("accessToken")) {
    } else {
      this.router.navigate(["/sign-in"]);
    }
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.userFullNam = sessionStorage.getItem('fullname');
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {}

  /**
   * Getter for current year
   */
  get currentYear(): number {
    return new Date().getFullYear();
  }

  // read info from smart card by secure agent service
  async readSamrtCard() {
    this.spinner.show();
    try {
      let rs: any = await this.secureAgentService.readSamrtCard();
      console.log(rs);
      this.select_cid = rs.data.pid;
      await this.search_cid(this.select_cid);
      this.spinner.hide();
    } catch (error) {
      this.spinner.hide();
      console.log(error);
    }
  }

  /**
   * search cid
   */
  async search_cid(i: any) {
    console.log("search_cid");

    this.spinner.show();
    this.select_cid = i;
    console.log("cid:", this.select_cid);
    this.is_display = false;
    this.visits = [];
    this.patient = [];
    this.allergy = [];
    this.selected_namecln = [];
    this.selected_dateserv = [];
    this.selected_timeserv = [];
    //vital_detail
    this.cc = [];
    this.pi = [];
    this.ph = [];
    this.pe = [];
    this.bmi = [];
    this.bt = [];
    this.dbp = [];
    this.sbp = [];
    this.pr = [];
    this.rr = [];
    this.wt = [];
    this.ht = [];
    this.o2sat = [];
    this.eye_score = [];
    this.gcs_score = [];
    this.verbal_score = [];
    this.vital_sign = [];
    this.diagnosis = [];
    this.xrays = [];
    this.drugs = [];
    this.procedures = [];
    this.labs = [];
    let isData: boolean = false;
    
    // save log search cid 
 
    try {
        let logs = await this.logsService.saveLogs(this.userFullNam,'Search CID : '+this.select_cid);
        const log_data = logs.data;
        // console.log(log_data);
    } catch (error) {
        console.log(error);
    }


    try {
      let rs: any = await this.listHospitalService.listHospital(i);
      // console.log(rs);
      if (rs.data.data.length > 0) {
        isData = true;
        this.hospitals = rs.data.data;
        this.patient = rs.data.profile_data[0];
        console.log('success get list hospital');
        this.spinner.hide();
      } else {
        this.spinner.hide();
        this.sweetAlertService.error(
          "คำชี้แจง",
          "ไม่พบข้อมูลผู้รับบริการ",
          "Smart EMR"
        );
      }
    } catch (error) {
      this.spinner.hide();
      console.log(error);
    }

    if (isData) {
      try {
        let rs: any = await this.allergyService.listAllergy(i);
        // console.log(rs);
        this.allergy = rs.data.rows;
        // console.log(this.allergy);
        this.spinner.hide();
      } catch (error) {
        this.spinner.hide();
        console.log(error);
      }
    }
  }

  /**
   * search hospital
   */
  async search_hospital(i: any) {
    this.spinner.show();

    this.select_hospcode = i;
    console.log("hospital:", this.select_hospcode);
    // this.visits = visit.filter((item:any) => item.hospcode == this.select_hospcode);
    try {
      let rs: any = await this.listVisitService.listVisit(
        this.select_hospcode,
        this.select_cid
      );
      console.log(rs.data);
      this.visits = await rs.data;
      console.log(this.visits);
      for (let v of this.visits) {
        v.date = this.changeDateFormat(v.date);
      }
      console.log(this.visits);
      this.spinner.hide();
    } catch (error) {
      this.spinner.hide();

      console.log(error);
    }
  }

  /**
   * search visit
   */
  async search_visit(i: any) {
    this.spinner.show();

    console.log(i);
    this.select_visit = i;
    console.log("visit:", this.select_visit);
    this.is_display = true;
    try {
      let rs: any = await this.listServiceProfileService.listServiceProfile(
        i.hospcode,
        i.hn,
        i.vn
      );
      console.log(rs);
      //patiest
      //   this.patient = rs.data.profile[0];
      this.selected_namecln = i.clinic;
      this.selected_dateserv = i.date;
      this.selected_timeserv = i.time;
      if (i.an) {
        this.is_admit = true;
      }
      //vital_detail
      this.cc = rs.data.visit_detail.cc;
      this.pi = rs.data.visit_detail.pi;
      this.ph = rs.data.visit_detail.ph;
      this.pe = rs.data.visit_detail.pe;
      this.bmi = rs.data.visit_detail.vital_sign.bmi;
      this.bt = rs.data.visit_detail.vital_sign.bt;
      this.dbp = rs.data.visit_detail.vital_sign.dbp;
      this.sbp = rs.data.visit_detail.vital_sign.sbp;
      this.pr = rs.data.visit_detail.vital_sign.pr;
      this.rr = rs.data.visit_detail.vital_sign.rr;
      this.wt = rs.data.visit_detail.vital_sign.wt;
      this.ht = rs.data.visit_detail.vital_sign.ht;
      this.o2sat = rs.data.visit_detail.vital_sign.o2sat;
      this.eye_score = rs.data.visit_detail.vital_sign.eye_score;
      this.gcs_score = rs.data.visit_detail.vital_sign.gcs_score;
      this.verbal_score = rs.data.visit_detail.vital_sign.verbal_score;

      //vital_sign
      this.vital_sign = rs.data.visit_detail.vital_sign;
      //diagnosis
      this.diagnosis = rs.data.diagnosis;
      //xray
      this.xrays = rs.data.xrays;
      if (this.xrays.length > 0) {
        for (let x of this.xrays) {
          if (x.date_serv == null) {
            x.date_serv = "ไม่ระบุวัน";
          } else {
            x.date_serv = this.changeDateFormat(x.date_serv);
          }
        }
      }
      //if an order
      this.orders = rs.data.orders;
      if (this.orders.length > 0) {
        let _date: any;
        for (let o of this.orders) {
          if (_date == o.date_start) {
            _date = o.date_start;
            o.is_same_date = true;
          } else {
            _date = o.date_start;
            o.is_same_date = false;
          }
          if (o.date_start == null) {
            o.date_start = "ไม่ระบุวัน";
          } else {
            o.date_start = this.changeDateFormat(o.date_start);
          }
          if (o.date_end == null) {
            o.date_end = "ไม่ระบุวัน";
          } else {
            o.date_end = this.changeDateFormat(o.date_end);
          }
        }
        console.log(this.orders);
      }

      //drug
      this.drugs = rs.data.drugs;
      if (this.drugs.length > 0) {
        for (let d of this.drugs) {
          if (d.date_serv == null) {
            d.date_serv = "ไม่ระบุวัน";
          } else {
            d.date_serv = this.changeDateFormat(d.date_serv);
          }
        }
      }
      //procedurs
      this.procedures = rs.data.procedures;
      if (this.procedures.length > 0) {
        for (let p of this.procedures) {
          if (p.date_serv == null) {
            p.date_serv = "ไม่ระบุวัน";
          } else {
            p.date_serv = this.changeDateFormat(p.date_serv);
          }
        }
      }

      //labs
      this.labs = rs.data.labs;
      if (this.labs.length > 0) {
        for (let l of this.labs) {
          if (l.date_serv == null) {
            l.date_serv = "ไม่ระบุวัน";
          } else {
            l.date_serv = this.changeDateFormat(l.date_serv);
          }
        }
      }
      this.spinner.hide();
    } catch (error) {
      this.spinner.hide();

      console.log(error);
    }
  }

  async change_layout() {
    this.is_admit = !this.is_admit;
  }

  changeDateFormat(date: any) {
    const month = [
      "ม.ค.",
      "ก.พ.",
      "มี.ค.",
      "เม.ย.",
      "พ.ค.",
      "มิ.ย.",
      "ก.ค.",
      "ส.ค.",
      "ก.ย.",
      "ต.ค.",
      "พ.ย.",
      "ธ.ค.",
    ];

    let inputDate = new Date(date);

    let dataDate = [
      inputDate.getDay(),
      inputDate.getDate(),
      inputDate.getMonth(),
      inputDate.getFullYear(),
    ];
    let outputDate = [
      dataDate[1],
      month[dataDate[2]],
      (dataDate[3] + 543).toString().substring(2, 4),
    ];
    return outputDate.join(" ");
  }
}
