
export interface Steptokinese
{
    hospcode?: string;
    cid?: string;
    date?: string;
    time?: string;
}

export interface Warfarin 
{
    hospcode?: string;
    cid?: string;
    date?: string;
}

export interface Allergy
{
    hospcode?: string;
    cid?: string;
    drug?: string;
    drugcode?: string;
    drugname?: string;
    symptom?: string;
    level?: string;
    date?: string;
}

export interface Profile
{
    cid?: string;
    name?: string;
    hn?: string;
    hospcode?: string;
    avatar?: string;
    dob?: string;
    sex?: string;
    age?: string; 
    address?: string;
    tel?: string;   
    steptokinese?: Steptokinese[];
    warfarin?: Warfarin[];
    allergy?: Allergy[];
}

export interface Visit
{
    hospcode?: string;
    hn?: string;
    vn?: string;
    date?: string;
    time?: string;
    clinic?: string;
}

export interface Diagnosis
{
    hospcode?: string;
    hn?: string;
    vn?: string;
    diag?: string;
    diagtype?: string;
    diagcode?: string;
    diagname?: string;
}

export interface Drug
{
    hospcode?: string;
    hn?: string;
    vn?: string;
    drug?: string;
    drugcode?: string;
    drugname?: string;
    qty?: string;
    unit?: string;
    druguse?: string;
}

export interface Lab
{
    hospcode?: string;
    hn?: string;
    vn?: string;
    lab?: string;
    labcode?: string;
    labname?: string;
    labresult?: string;
    labunit?: string;
    labrange?: string;
}

export interface Procedure
{
    hospcode?: string;
    hn?: string;
    vn?: string;
    procedure?: string;
    procedurecode?: string;
    procedurename?: string;
}

export interface OpdCard
{
    visit: Visit,
    profile: Profile,
    diagnosis: Diagnosis[],
    drug: Drug[],
    lab: Lab[],
    procedure: Procedure[],
}
