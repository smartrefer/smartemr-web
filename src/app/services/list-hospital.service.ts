import { Injectable } from '@angular/core'
import axios from 'axios'
import { environment } from '../../environments/environment'


@Injectable({
  providedIn: 'root'
})
export class ListHospitalService {
    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}/query`
      })
    
      constructor () {
        this.axiosInstance.interceptors.request.use(config => {
          const token = sessionStorage.getItem('token')
          if (token) {
            config.headers['Authorization'] = `Bearer ${token}`
          }
          return config
        });
    
        this.axiosInstance.interceptors.response.use(response => {
          return response
        }, error => {
          return Promise.reject(error)
        })
      }
    
      async listHospital(data: any) {
        const url = `/hospital/${data}`
        return await this.axiosInstance.get(url)
      }

}