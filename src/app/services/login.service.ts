  import { Injectable } from '@angular/core'
  import axios from 'axios'
  import { environment } from '../../environments/environment'
  
  
  @Injectable({
    providedIn: 'root'
  })
  export class LoginService {
      private axiosInstance = axios.create({
          baseURL: `${environment.apiUrl}`
        })
      
        constructor () {
        }
        //แสดง Visit ที่มารับริการตามโรงพยาบาลที่เลือก
        async login(info: any) {
          const url = `/login`
          return await this.axiosInstance.post(url,info)
        }
  
  }