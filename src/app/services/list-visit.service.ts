import { Injectable } from '@angular/core'
import axios from 'axios'
import { environment } from '../../environments/environment'


@Injectable({
  providedIn: 'root'
})
export class ListVisitService {
    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}/query`
      })
    
      constructor () {
        this.axiosInstance.interceptors.request.use(config => {
          const token = sessionStorage.getItem('token')
          if (token) {
            config.headers['Authorization'] = `Bearer ${token}`
          }
          return config
        });
    
        this.axiosInstance.interceptors.response.use(response => {
          return response
        }, error => {
          return Promise.reject(error)
        })
      }
      //แสดง Visit ที่มารับริการตามโรงพยาบาลที่เลือก
      async listVisit(hospcode: any,cid: any) {
        const url = `/service/${hospcode}/${cid}`
        return await this.axiosInstance.get(url)
      }

}